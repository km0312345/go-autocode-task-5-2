package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	val1, err := myStrToInt("1")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	val2, err := myStrToInt2("1")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	if val1 != val2 {
		fmt.Fprintf(os.Stderr, "%d!=%d\n", val1, val2)
		os.Exit(1)
	}
	fmt.Printf("%d=%d\n", val1, val2)
}

func myStrToInt(s string) (int, error) {
	return strconv.Atoi(s)
}

func myStrToInt2(s string) (int, error) {
	val, err := strconv.ParseInt(s, 10, 64)
	return int(val), err
}
